(define (compose f g) (lambda (x) (f (g x))))
(define (identity x) x)

(define (any? L)
  (and (not (null? L))
       (or (car L) (any? (cdr L)))))

(define (all? L)
  (and (not (null? L))
       (car L)
       (or (null? (cdr L)) (all? (cdr L)))))

(define (bounded-random low high)
  (let ((low (min low high))
        (high (max low high)))
    (+ low (random (+ 1 (- high low))))))

(define (displayln . L)
  (define (helper L)
    (if (null? L)
        (newline)
        (begin (display (car L)) (display " ")
               (helper (cdr L)))))
  (helper L))

(define (flatmap f L)
  (fold-right append '() (map f L)))

(define (flatten L)
  (cond ((null? L) L)
        ((pair? (car L)) (append (flatten (car L)) (flatten (cdr L))))
        (else (cons (car L) (flatten (cdr L))))))

(define (none? L) (not (any? L)))

(define (qrange m n)   ; no special cases ("quick")
  (define (iter m n so-far)
    (if (> m n) so-far (iter m (- n 1) (cons n so-far))))
  (iter m n '()))

(define (range m . n)  ; accommodate special cases
  (if (null? n)
      (qrange (min 1 m) (max 1 m))
      (let ((n (car n)))
        (if (<= m n)
            (qrange m n)
            (reverse (qrange n m))))))

(define (symbol-append . L)
  (string->symbol (fold-left string-append "" (map symbol->string L))))

(define (unique-randoms count max-value)
  (define (iter count L)
    (let ((x (+ 1 (random max-value))))
      (cond ((< count 1) L)
            ((memq x L) (iter count L))
            (else (iter (- count 1) (cons x L))))))
  (iter count '()))
