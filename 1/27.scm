; prime?

(define (divides? x y) (= (remainder y x) 0))
(define (next n) (if (= n 2) 3 (+ n 2)))
(define (square x) (* x x))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))

(define (smallest-divisor x) (find-divisor x 2))
(define (prime? x) (= x (smallest-divisor x)))

; fermat-test-all?

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
        (else (remainder (* base (expmod base (- exp 1) m)) m))))

(define (fermat-test-all? n)
  (define (congruent? a) (= (expmod a n n) a))
  (define (all-congruent? count)
    (cond ((> count n) (error 'fermat-test-all? "Logic error"))
          ((= count n) #t)
          ((congruent? count) (all-congruent? (+ count 1)))
          (else #f)))
  (all-congruent? 1))

; Show values

(define carmichael-numbers '(561 1105 1729 2465 2821 6601))

(begin
  (display "prime? ............. ")
  (display (map prime? carmichael-numbers)) (newline)
  (display "fermat-test-all? ... ")
  (display (map fermat-test-all? carmichael-numbers)))
