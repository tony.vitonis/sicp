(define (cont-frac n d k)
  (define (iter count so-far)
    (if (= count 0)
        so-far
        (iter (- count 1) (/ (n count) (+ so-far (d count))))))
  (iter k 0))

(define (cont-frac-recurse n d k)
  (define (inner count)
    (if (> count k)
        0
        (/ (n k) (+ (d k) (inner (+ count 1))))))
  (inner 1))

; --------------------

(define (test f)
  (define test-value 0.618034)
  (define (one _) 1.0)
  (define (iter count)
    (let ((this (f one one count)))
      (if (< (abs (- this test-value)) 1e-4)
          (begin (display f) (newline)
                 (display count) (display ": ") (display this) (newline))
          (iter (+ count 1)))))
  (iter 1))

(test cont-frac)
(test cont-frac-recurse)
