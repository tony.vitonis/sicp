(define sin
  (trace-lambda sin (x)
    (define tolerance 0.1)
    (define (within-tolerance? x)
      (< (abs x) tolerance))
    (define (cube x) (* x x x))
    (define (p x)
      (- (* 3 x) (* 4 (cube x))))
    (if (within-tolerance? x)
        x
        (p (sin (/ x 3.0))))))

(sin 12.15)
