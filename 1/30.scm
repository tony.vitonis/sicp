(define (sum-recurse term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum-recurse term (next a) next b))))

(define (sum term a next b)
  (define (iter a so-far)
    (if (> a b)
        so-far
        (iter (next a) (+ so-far (term a)))))
  (iter a 0))

(define (identity x) x)
(define (inc n) (+ n 1))
(define (cube n) (* n n n))

(define (sum-cubes a b) (sum cube a inc b))
(define (sum-integers a b) (sum identity a inc b))

(define (sum-pi a b)
  (define (term x) (/ 1.0 (* x (+ x 2))))
  (define (next x) (+ x 4))
  (sum term a next b))

(define (integral f a b dx)
  (define (next x) (+ x dx))
  (* dx (sum f (+ a (/ dx 2.0)) next b)))

(display (list (sum-cubes 1 10)
               (sum-integers 1 10)
               (* 8 (sum-pi 1 100000))
               (integral cube 0 1 0.01)
               (integral cube 0 1 0.001)))
