(define tolerance 1e-15)

; --------------------

(define (average a b) (/ (+ a b) 2))
(define (close-enough? a b) (< (abs (- a b)) tolerance))
(define (square x) (* x x))

; --------------------

(define (iterative-improve acceptable? improve)
  (define (try guess)
    (let ((next (improve guess)))
      (if (acceptable? guess next)
          next
          (try next))))
  try)

; --------------------

(define (fixed-point f first-guess)
  ((iterative-improve close-enough? f) first-guess))

(define (sqrt x)
  ((iterative-improve close-enough?
                      (lambda (y) (average y (/ x y))))
   1.0))

; --------------------

(define cos-fp (fixed-point cos 1.0))
(define sqrt2 (sqrt 2))

(printf "~a should be very close to ~a\n" cos-fp (cos cos-fp))
(printf "~a squared is ~a" sqrt2 (square sqrt2))
