(define (cont-frac n d k)
  (define (iter count so-far)
    (if (= count 0)
        so-far
        (iter (- count 1) (/ (n count) (+ so-far (d count))))))
  (iter k 0))

; --------------------

(define (euler-e k)
  (define (n _) 1.0)
  (define (d k)
    (if (= 0 (remainder (+ k 1) 3))
        (* 2 (/ (+ k 1) 3))
        1))
  (+ 2 (cont-frac n d k)))

; --------------------

(display (list (euler-e 1)
               (euler-e 10)
               (euler-e 100)))
