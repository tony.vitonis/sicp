(define (compose f g) (lambda (x) (f (g x))))

(define (average . L)
  (define (sum L)
    (if (null? L) 0.0
                  (+ (car L) (sum (cdr L)))))
  (/ (sum L) (length L)))

(define (smooth f)
  (let ((dx 1e-10))
    (lambda (x) (average (f (- x dx)) (f x) (f (+ x dx))))))

(define (repeated f n)
  (if (= n 1)
      f
      (compose f (repeated f (- n 1)))))

(define (n-fold-smooth f n)
  ((repeated smooth n) f))
