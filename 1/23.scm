(define (divides? x y) (= (remainder y x) 0))
(define (square x) (* x x))
(define (smallest-divisor x) (find-divisor x 2))
(define (prime? x) (= x (smallest-divisor x)))

(define (next n) (if (= n 2) 3 (+ n 2)))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))

(define (primes-starting-at n)
  (define (iter n count runtime)
    (if (< count 3)
      (if (prime? n)
          (begin (display n)
                 (display " (") (display (- (cpu-time) runtime)) (display " ms)")
                 (newline)
                 (iter (+ n 1) (+ count 1) (cpu-time)))
          (iter (+ n 1) count runtime))))
  (iter n 0 (cpu-time)))

(define (test n count)
    (display "primes starting at ") (display n) (newline)
    (primes-starting-at n)
    (if (> count 0) (test (* n 10) (- count 1))))

(test 10000000000 4)
