(define (square n)
  (display "squaring ") (display n) (newline)
  (* n n))

(define (fast-expt b n)
   (cond ((= n 0) 1)
         ((even? n) (square (fast-expt b (/ n 2))))
         (else (* b (fast-expt b (- n 1))))))

(define (expmod base exp m)
  (display "expmod ") (display (list base exp m)) (newline)
  (cond ((= exp 0) 1)
        ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
        (else (remainder (* base (expmod base (- exp 1) m)) m))))

(define (alyssa base exp m)
  (display "alyssa ") (display (list base exp m)) (newline)
  (remainder (fast-expt base exp) m))

(list (expmod 1234567 23456 23) (alyssa 1234567 23456 23)
      (expmod 1234567 23456 1000001) (alyssa 1234567 23456 1000001))
