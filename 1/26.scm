(define (square n)
  (display "squaring ") (display n) (newline)
  (* n n))

(define (fast-expt b n)
   (cond ((= n 0) 1)
         ((even? n) (square (fast-expt b (/ n 2))))
         (else (* b (fast-expt b (- n 1))))))

(define expmod
  (trace-lambda expmod (base exp m)
    (cond ((= exp 0) 1)
          ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
          (else (remainder (* base (expmod base (- exp 1) m)) m)))))

(define louis
  (trace-lambda louis (base exp m)
    (cond ((= exp 0) 1)
          ((even? exp) (remainder (* (louis base (/ exp 2) m)
                                     (louis base (/ exp 2) m))
                                  m))
          (else (remainder (* base (louis base (- exp 1) m)) m)))))

(list (expmod 10 5 3)
      (louis 10 5 3))
