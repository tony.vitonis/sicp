(define (double x) (+ x x))
(define (halve x) (/ x 2))
(define (even? x) (= (remainder x 2) 0))

(define (my* m n)
  (cond ((or (= m 0) (= n 0)) 0)
        ((even? n) (my* (double m) (halve n)))
        (else (+ m (my* m (- n 1))))))
