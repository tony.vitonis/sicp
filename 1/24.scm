(define (square x) (* x x))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
        (else (remainder (* base (expmod base (- exp 1) m)) m))))

(define (fermat-test n)
  (define (tryit a)
    (= (expmod a n n) a))
  (tryit (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) #t)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else #f)))

(define (primes-starting-at n)
  (define (iter n count runtime)
    (if (< count 3)
      (if (fast-prime? n 10)
          (begin (display n)
                 (display " (") (display (- (cpu-time) runtime)) (display " ms)")
                 (newline)
                 (iter (+ n 1) (+ count 1) (cpu-time)))
          (iter (+ n 1) count runtime))))
  (iter n 0 (cpu-time)))

(define (test n count)
    (display "primes starting at ") (display n) (newline)
    (primes-starting-at n)
    (if (> count 0) (test (* n 10) (- count 1))))

(test 10000000000 7)
