; a is the accumulator
; m is the multiplicand
; n is the multiplier
;
; If n is even: a + mn = a + [(2m)(n/2)]
;   new a is a; new m is 2m; new n is n/2
; If n is odd:  a + mn = a + m[1+(n-1)] = (a+m) + [m(n-1)]
;   new a is a+m; new m is m ; new n is n-1

(define (double x) (+ x x))
(define (halve x) (/ x 2))
(define (even? x) (= (remainder x 2) 0))

(define (my* m n)
  (define (iter a m n)
    (cond ((or (= m 0) (= n 0)) a)
          ((even? n) (iter a (double m) (halve n)))
          (else (iter (+ a m) m (- n 1)))))
  (iter 0 m n))
