; a is the accumulator
; b is the base
; x is the exponent
;
; If x is even: ab^x = a(b^2)^x/2
;    new a is a, new b is b^2, new x is x/2
; If x is odd:  ab^x = ab(b^(x-1))
;    new a is ab, new b is b, new x is x-1

(define (square x) (* x x))
(define (even? x) (= (remainder x 2) 0))

(define (fast-expt b x)
   (define (iter a b x)
      (cond ((= x 0) a)
            ((even? x) (iter a (square b) (/ x 2)))
            (else (iter (* a b) b (- x 1)))))
   (iter 1 b x))
