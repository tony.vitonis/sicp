(define (cont-frac n d k)
  (define (iter count so-far)
    (if (= count 0)
        so-far
        (iter (- count 1) (/ (n count) (+ so-far (d count))))))
  (iter k 0))

; --------------------

(define (tan-cf x k)
  (define (n k) (if (= k 1) x (- (* x x))))
  (define (d k) (- (* k 2) 1.0))
  (cont-frac n d k))

; --------------------

(define pi (* 4 (atan 1.0)))

(define test1 (/ pi 3))
(define test2 (/ pi 4))
(define test3 (/ pi 6))

(display (list (tan-cf test1 1) (tan-cf test1 10) (tan test1))) (newline)
(display (list (tan-cf test2 1) (tan-cf test2 10) (tan test2))) (newline)
(display (list (tan-cf test3 1) (tan-cf test3 10) (tan test3))) (newline)
