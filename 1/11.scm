(define (f-recursive n)
  (if (< n 3)
      n
      (+ (f-recursive (- n 1))
         (* 2 (f-recursive (- n 2)))
         (* 3 (f-recursive (- n 3))))))

(define (f-iterative n)
  (define (iter n-3 n-2 n-1 count)
    (cond ((< count 3) count)
          ((= count 3) n-1)
          (else (iter n-2
                      n-1
                      (+ n-1 (* 2 n-2) (* 3 n-3))
                      (- count 1)))))
  (iter 1 2 4 n))
