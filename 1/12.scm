; No error checking ...

(define (pascal row col)
  (cond ((= col 1) 1)
        ((= col row) 1)
        (else (+ (pascal (- row 1) (- col 1))
                 (pascal (- row 1) col)))))

(define (pascal-triangle rows)
  (define (iter-col row col)
    (if (<= col row)
      (begin (display (pascal row col))
             (when (< col row) (display " "))
             (iter-col row (+ col 1)))))
  (define (iter-row row rows)
    (if (<= row rows)
      (begin (iter-col row 1)
             (newline)
             (iter-row (+ row 1) rows))))
  (iter-row 1 rows))

(pascal-triangle 10)
