(define (product-recurse term a next b)
  (if (> a b)
      1
      (* (term a)
         (product-recurse term (next a) next b))))

(define (product term a next b)
  (define (iter a so-far)
    (if (> a b)
        so-far
        (iter (next a) (* so-far (term a)))))
  (iter a 1))

(define (displayln thing) (display thing) (newline))
(define (identity x) x)
(define (inc n) (+ n 1))

(define (factorial-recurse n)
  (product-recurse identity 1 inc n))

(define (factorial n)
  (product identity 1 inc n))

(define (wallis-pi n)
  (define (term t)
    (if (odd? t) (/ (+ t 1) (+ t 2))
                 (/ (+ t 2) (+ t 1))))
  (* 4 (product term 1.0 inc n)))

(define (test)
  (let ((n (+ 20 (random 31))))
    (displayln n)
    (displayln (factorial n))
    (displayln (factorial-recurse n))))

(displayln (wallis-pi 1000000))
(test)
