(load "../math.scm")

(define (always _) #t)
(define (identity x) x)

; --------------------

(define (filtered-accumulate match? combiner null-value term a next b)
  (define (iter a so-far)
    (cond ((> a b) so-far)
          ((match? a) (iter (next a) (combiner so-far (term a))))
          (else (iter (next a) so-far))))
  (iter a null-value))

(define (factorial n)
  (filtered-accumulate always * 1 identity 1 inc n))

(define (sum-of-squares-of-primes m n)
  (filtered-accumulate prime? + 0 square m inc n))

(define (product-of-relative-primes n)
  (define (relative-prime? m) (= 1 (gcd m n)))
  (filtered-accumulate relative-prime? * 1 identity 2 inc n))
