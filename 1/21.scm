(define (divides? x y) (= (remainder y x) 0))
(define (square x) (* x x))
(define (smallest-divisor x) (find-divisor x 2))
(define (prime? x) (= x (smallest-divisor x)))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(display (list (list 199 (smallest-divisor 199))
               (list 1999 (smallest-divisor 1999))
               (list 19999 (smallest-divisor 19999))))
