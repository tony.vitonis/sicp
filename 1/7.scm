(define (average x y) (/ (+ x y) 2))

(define (sqrt x)
  (define tolerance 1e-16)
  (define (within-tolerance? x y)
    (< (abs (- x y)) tolerance))
  (define (improve guess)
    (average guess (/ x guess)))
  (define (good-enough? guess)
    (within-tolerance? guess (improve guess)))
  (define (iter guess)
    (if (good-enough? guess)
      guess
      (iter (improve guess))))
  (iter 1.0))
