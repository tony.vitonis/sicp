(define tolerance 1e-10)

(define (average m n) (/ (+ m n) 2))

; --------------------

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (display "guess is ") (display guess) (newline)
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (average-damp f)
  (lambda (x) (average x (f x))))

; --------------------

(define (x-to-the-x n)
  (fixed-point (lambda (x) (/ (log n) (log x))) 2.0))

(define (damped-x-to-the-x n)
  (fixed-point (average-damp (lambda (x) (/ (log n) (log x)))) 2.0))

; --------------------

(damped-x-to-the-x 1000)
(display "--------------------") (newline)
(x-to-the-x 1000)
