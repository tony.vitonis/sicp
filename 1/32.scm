(define (cube n) (* n n n))
(define (displayln item) (display item) (newline))
(define (identity x) x)
(define (inc n) (+ n 1))

; --------------------

(define (accumulate combiner null-value term a next b)
  (define (iter a so-far)
    (if (> a b)
        so-far
        (iter (next a) (combiner so-far (term a)))))
  (iter a null-value))

(define (accumulate-recurse combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
                (product-recurse term (next a) next b))))

; --------------------

(define (sum term a next b)             (accumulate + 0 term a next b))
(define (sum-recurse term a next b)     (accumulate-recurse + 0 term a next b))
(define (product term a next b)         (accumulate * 1 term a next b))
(define (product-recurse term a next b) (accumulate-recurse * 1 term a next b))

(define (factorial n)         (product identity 1 inc n))
(define (factorial-recurse n) (product-recurse identity 1 inc n))
(define (sum-cubes a b)       (sum cube a inc b))
(define (sum-integers a b)    (sum-recurse identity a inc b))

; --------------------

(define (integral f a b dx)
  (define (next x) (+ x dx))
  (* dx (sum f (+ a (/ dx 2.0)) next b)))

(define (sum-pi a b)
  (define (term x) (/ 1.0 (* x (+ x 2))))
  (define (next x) (+ x 4))
  (sum term a next b))

(define (wallis-pi n)
  (define (term t)
    (if (odd? t) (/ (+ t 1) (+ t 2))
                 (/ (+ t 2) (+ t 1))))
  (* 4 (product term 1.0 inc n)))

; --------------------

(define n (+ 20 (random 31)))

(displayln (sum-cubes 1 10))
(displayln (sum-integers 1 10))
(displayln (* 8 (sum-pi 1 100000)))
(displayln (integral cube 0 1 0.01))
(displayln (integral cube 0 1 0.001))))
(displayln n)
(displayln (factorial n))
(displayln (factorial-recurse n))))
(displayln (wallis-pi 1000000))
