(define float exact->inexact)

(define (coefficient k n)
  (cond ((= k 0) 1)
        ((= k n) 1)
        ((even? k) 4)
        (else 2)))

(define (term f a n k h) (f (+ a (* h (coefficient k n)))))

(define (simpson f a b n)
  (let ((h (float (/ (- b a) n))))
    (define (term k) (* (coefficient k n) (f (+ a (* h k)))))
    (define (iter f a n k so-far)
      (if (> k n)
          so-far
          (iter f a n (+ k 1) (+ so-far (term k)))))
    (* (/ h 3) (iter f a n 0 0))))

(define (cube x) (* x x x))

(define (test times)
  (list times (simpson cube 0 1 times)))

(list (test 10)
      (test 100)
      (test 1000)
      (test 10000)
      (test 100000)
      (test 1000000))
