(define tolerance 1e-10)

; --------------------

(define (average m n) (/ (+ m n) 2))
(define (average-damp f) (lambda (x) (average x (f x))))
(define (compose f g) (lambda (x) (f (g x))))
(define (log2 n) (/ (log n) (log 2)))
(define (sgn n) (cond [(> n 0) 1] [(= n 0) 0] [else -1]))

; --------------------

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (repeated f n)
  (if (= n 1)
      f
      (compose f (repeated f (- n 1)))))

(define (nth-root x n)
  (define (damps n)
    (if (< n 3) 1
                (+ (truncate (log2 (- n 1))) 1)))
    (fixed-point ((repeated average-damp (damps n)) (lambda (y) (/ x (expt y (- n 1)))))
                 1.0))

; --------------------

(define (make-procedures m)
  (define (iter L n)
    (define (p x) (nth-root x (+ n 1)))
    (if (> n m)
        L
        (iter (cons p L) (+ n 1))))
  (reverse (iter '() 1)))

(define (test procedures n)
  (define (show value root)
    (printf "~a^~a = ~a" value root (expt value root))
    (newline))
  (define (iter procedures n root)
    (unless (null? procedures)
      (show ((car procedures) n) root)
      (iter (cdr procedures) n (+ root 1))))
  (iter procedures n 2))

(test (make-procedures 16) 8675309)
