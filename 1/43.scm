(define (compose f g) (lambda (x) (f (g x))))
(define (square x) (* x x))
(define (halve x) (/ x 2.0))

(define (repeated f n)
  (if (= n 1)
      f
      (compose f (repeated f (- n 1)))))

(display (list ((repeated square 2) 5)
               ((repeated halve 5) 8)))
