(define (average x y) (/ (+ x y) 2))
(define (cube x) (* x x x))
(define (square x) (* x x))

(define tolerance 1e-10)

; --------------------

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (deriv f)
  (let ((dx 1e-10))
    (lambda (x)
      (/ (- (f (+ x dx)) (f x))
         dx))))

(define (newton-transform f)
  (lambda (x)
    (- x (/ (f x) ((deriv f) x)))))

(define (newtons-method f guess)
  (fixed-point (newton-transform f) guess))

(define (cubic a b c)
  (lambda (x)
    (+ (cube x) (* a (square x)) (* b x) c)))

; --------------------

(define (sqrt x)
  (newtons-method (lambda (y) (- (square y) x))
                  1.0))

(define (cube-root x)
  (newtons-method (lambda (y) (- (cube y) x))
                  1.0))

(define (cubic-zero a b c x)
  (newtons-method (lambda (y) (- ((cubic a b c) y) x))
                  1.0))

; --------------------

(define a (sqrt 2))
(define b (cube-root 2))
(define c (cubic-zero 2 3 4 5))  ; x^3 + 2x^2 + 3x + 4 = 5

(display (list a (square a))) (newline)
(display (list b (cube b))) (newline)
(display (list c ((cubic 2 3 4) c)))
