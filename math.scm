(define phi (/ (+ 1 (sqrt 5)) 2))
(define pi (* 4 (atan 1.0)))
(define tolerance 1e-10)

; --------------------

(define (average m n) (/ (+ m n) 2))
(define (average-damp f) (lambda (x) (average x (f x))))
(define (closest-int n) (truncate (+ n 0.5)))
(define (divisible-by? n d) (zero? (remainder n d)))
(define (cube n) (* n n n))
(define (dec n) (- n 1))
(define (inc n) (+ n 1))
(define (sgn n) (cond [(positive? n) 1] [(negative? n) -1] [else 0]))
(define (square n) (* n n))

; --------------------

(define (close-enough? a . b)
  (if (null? b) (< (abs a) tolerance)
                (close-enough? (- a (car b)))))

(define (derivative f)
  (let ((dx tolerance))
    (lambda (x)
      (/ (- (f (+ x dx)) (f x))
         dx))))

(define (fixed-point f first-guess)
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (newtons-method f guess)
  (fixed-point (newton-transform f) guess))

(define (newton-transform f)
  (lambda (x) (- x (/ (f x) ((derivative f) x)))))

; --------------------

(define (cube-root x)
  (newtons-method (lambda (y) (- (cube y) x))
                  1.0))

(define (sqrt x)
  (newtons-method (lambda (y) (- (square y) x))
                  1.0))

; --------------------

(define (dround n d)
  (let ((m (expt 10 d)))
    (/ (round (* n m)) m)))

(define (factorial n)
  (if (< n 2)
      1
      (* n (factorial (- n 1)))))

(define (fib n)
  (define (iter a b p q i)
    (cond ((= i 0) b)
          ((even? i) (iter a
                           b
                           (+ (square p) (square q))
                           (+ (square q) (* 2 p q))
                           (/ i 2)))
          (else (iter (+ (* b q) (* a q) (* a p))
                      (+ (* b p) (* a q))
                      p
                      q
                      (- i 1)))))
  (iter 1 0 0 1 n))

(define (prime? n)
  (let ((factors (prime-factors n)))
    (and (not (null? factors))
         (= n (car factors)))))

(define (prime-factors n)
  (define (next n) (if (= n 2) 3 (+ n 2)))
  (define (iter n i L)
    (cond ((< n i) (reverse L))
          ((divisible-by? n i) (iter (/ n i) i (cons i L)))
          (else (iter n (next i) L))))
  (if (< n 2) '()
              (iter n 2 '())))
