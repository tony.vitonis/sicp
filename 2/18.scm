(define (my-reverse L)
  (if (null? L)
      L
      (append (my-reverse (cdr L)) (list (car L)))))

(define (test L)
  (printf "(my-reverse ~a) -> ~a\n"
          L (my-reverse L)))

(test '(1 2 3 4 5))
(test '(test '(1 2 3 4 5)))
