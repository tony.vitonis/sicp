(define (double n) (+ n n))
(define (inc n) (+ n 1))
(define (square n) (* n n))

; --------------------

(define zero (lambda (f) (lambda (x) x)))
(define one (lambda (f) (lambda (x) (f x))))
(define two (lambda (f) (lambda (x) (f (f x)))))
(define three (lambda (f) (lambda (x) (f (f (f x))))))

; --------------------

(define (test f n)
  (display (list ((zero f) n)
                 ((one f) n)
                 ((two f) n)
                 ((three f) n)))
  (newline))

(test inc 0)
(test square 2)
(test double 5)
