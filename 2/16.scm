(define f (lambda (x) x))
(define g (lambda (x) (/ 1 (/ 1.0 x))))

(display (list (f 1000000000000001) (g 1000000000000001)))
