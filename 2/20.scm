; Must use define instead of let for match? because all defines must come first.

(define (same-parity f . L)
  (define match? (if (even? f) even? odd?))
  (define (iter L result)
    (if (null? L)
        (reverse result)
        (iter (cdr L)
              (if (match? (car L))
                  (cons (car L) result)
                  result))))
    (iter L (list f)))

; --------------------

(printf "~a\n" (same-parity 1 2 3 4 5 6 7))
(printf "~a\n" (same-parity 2 3 4 5 6 7))
