(define (=number? expr num) (and (number? expr) (= expr num)))
(define (=zero? expr) (=number? expr 0))
(define (=one? expr) (=number? expr 1))

(define (all? L)
  (or (null? L)
      (and (car L) (all? (cdr L)))))

(define (check-all f L) (all? (map f L)))
(define (numbers? . L) (check-all number? L))

(define (op-of expr) (car expr))
(define (lhs-of expr) (cadr expr))
(define (rhs-of expr) (caddr expr))
(define (=op? expr t) (and (pair? expr) (eq? (op-of expr) t)))

(define (variable? expr) (symbol? expr))

(define (same-variable? v1 v2)
  (and (variable? v1)
       (variable? v2)
       (eq? v1 v2)))

; --------------------

(define (d+ L R)
  (cond ((=zero? L) R)
        ((=zero? R) L)
        ((numbers? L R) (+ L R))
        (else (list '+ L R))))

(define (d- L R)
  (cond ((=zero? L) (- R))
        ((=zero? R) L)
        ((numbers? L R) (- L R))
        (else (list '- L R))))

(define (d* L R)
  (cond ((or (=zero? L) (=zero? R)) 0)
        ((=one? L) R)
        ((=one? R) L)
        ((numbers? L R) (* L R))
        (else (list '* L R))))

(define (d/ L R)
  (cond ((=zero? R) (error 'd/ "invalid zero denominator"))
        ((=zero? L) 0)
        ((=one? R) L)
        ((numbers? L R) (/ L R))
        (else (list '/ L R))))

(define (d^ L R)
  (cond ((not (number? R)) (error 'd^ "non-numeric exponent"))
        ((=zero? R) 1)
        ((=one? R) L)
        ((numbers? L R) (expt L R))
        (else (list '^ L R))))

; --------------------

(define (deriv expr var)
  (define (u) (lhs-of expr))
  (define (v) (rhs-of expr))
  (define (du) (deriv (u) var))
  (define (dv) (deriv (v) var))
  (define (udv) (d* (u) (dv)))
  (define (vdu) (d* (v) (du)))
  (define (op-is? x) (eq? (op-of expr) x))
  (cond ((number? expr) 0)
        ((variable? expr)
         (if (same-variable? expr var) 1 0))
        ((op-is? '+) (d+ (du) (dv)))
        ((op-is? '-) (d- (du) (dv)))
        ((op-is? '*) (d+ (udv) (vdu)))
        ((op-is? '/) (d/ (d- (vdu) (udv))
                         (d* (v) (v))))
        ((op-is? '^) (d* (d* (v) (d^ (u) (d- (v) 1)))
                         (du)))
        (else (error 'deriv "unknown operation type" (op-of expr)))))

; --------------------

(define (test expr e)
  (printf (string-append "Test:     ~a\n"
                         "Expected: ~a\n"
                         "Actual:   ~a\n\n")
           expr e (deriv expr 'x)))

(test '(+ (* 1/2 (^ x 2)) (* x 2))
      '(+ (* 1/2 (* 2 x)) 2))
(test '(- (^ x 3) (* 4 x))
      '(- (* 3 (^ x 2)) 4))
(test '(/ (* x 5) (+ x 3))
      '(/ (- (* (+ x 3) 5) (* x 5)) (* (+ x 3) (+ x 3))))
(test '(- (+ (* 4 (^ x 2)) (* 3 x)) 2)
      '(+ (* 4 (* 2 x)) 3))
