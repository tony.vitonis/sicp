(define (mycons x y)
  (lambda (m) (m x y)))

(define (mycar z)
  (z (lambda (p q) p)))

(define (mycdr z)
  (z (lambda (p q) q)))

; --------------------

(define x (mycons 2 3))
(display (list (mycar x) (mycdr x)))
(newline)

(define y (mycons 1 x))
(display (list (mycar y) (mycar (mycdr y)) (mycdr (mycdr y))))
(newline)
