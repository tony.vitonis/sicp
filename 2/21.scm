(load "../math.scm")

; --------------------

(define (square-list-recursive L)
  (if (null? L)
      L
      (cons (square (car L)) (square-list-recursive (cdr L)))))

(define (square-list-iterative L)
  (define (iter L result)
    (if (null? L)
        (reverse result)
        (iter (cdr L) (cons (square (car L)) result))))
  (iter L '()))

(define (square-list-map L)
  (map square L))

; --------------------

(printf "~a\n" (square-list-recursive '(2 3 5 7 11 13 17 19)))
(printf "~a\n" (square-list-iterative '(2 3 5 7 11 13 17 19)))
(printf "~a\n" (square-list-map       '(2 3 5 7 11 13 17 19)))
