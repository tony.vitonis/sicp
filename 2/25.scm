(define L1 '(1 3 (5 7) 9))
(define L2 '((7)))
(define L3 '(1 (2 (3 (4 (5 (6 7)))))))

(define (displayln x) (printf "~a\n" x))

(displayln (car (cdaddr L1)))
(displayln (caar L2))
(displayln (cadadr (cadadr (cadadr L3))))
