(define (interval a b) (cons (min a b) (max a b)))

(define (center-percent c p) (center-tolerance c (/ p 100)))
(define (center-tolerance c t) (center-width c (* c t)))
(define (center-width c w) (interval (- c w) (+ c w)))

(define (lower-bound i) (car i))
(define (upper-bound i) (cdr i))

(define (center i) (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (percent i) (* 100 (tolerance i)))
(define (span i) (- (upper-bound i) (lower-bound i)))
(define (tolerance i) (/ (width i) (abs (center i))))
(define (width i) (/ (span i) 2))

; --------------------

(define (interval+ x y)
  (interval (+ (lower-bound x) (lower-bound y))
            (+ (upper-bound x) (upper-bound y))))

(define (interval- x y)
  (interval (- (lower-bound x) (upper-bound y))
            (- (upper-bound x) (lower-bound y))))

(define (interval* i j)
  (let ((p1 (* (lower-bound i) (lower-bound j)))
        (p2 (* (lower-bound i) (upper-bound j)))
        (p3 (* (upper-bound i) (lower-bound j)))
        (p4 (* (upper-bound i) (upper-bound j))))
    (interval (min p1 p2 p3 p4)
              (max p1 p2 p3 p4))))

(define (interval/ x y)
  (if (negative? (* (lower-bound y) (upper-bound y)))
      (error 'interval* "denominator interval cannot span 0")
      (interval* x (interval (/ 1 (upper-bound y))
                             (/ 1 (lower-bound y))))))

(define (interval-1/ i)
  (interval/ (interval 1 1) i))

; --------------------

(define (interval-repr i)
  (format "(~a,~a)" (car i) (cdr i)))

(define (interval-trepr i)
  (define (dround n d)
    (let ((m (expt 10 d)))
      (/ (round (* n m)) m)))
  (format "(~a ± ~a%)"
          (center i) (dround (percent i) 5)))
