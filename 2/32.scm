(define (subsets s)
  (if (null? s)
      (list s)
      (let ((rest (subsets (cdr s))))
        (append rest (map (lambda (L) (cons (car s) L))
                          rest)))))

(printf "~a\n" (subsets '(1 2 3)))
(printf "~a\n" (subsets '(a b c d)))
