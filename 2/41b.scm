(load "../util.scm")  ; flatmap, qrange

; --------------------

(define (nth-order n smaller)
  (define (one-set low high)
    (map (lambda (x) (cons low x))
         (smaller (+ low 1) high)))
  (lambda (low high)
    (flatmap (lambda (x) (one-set x high))
             (qrange low (- high (- n 1))))))

(define (k-combinations k)
  (if (= k 0) (lambda (low high) '(()))
              (nth-order k (k-combinations (- k 1)))))

; --------------------

; Combinations of k numbers from m to n that add up to s

(define (matching-sums k m n s)
  (filter (lambda (x) (= (fold-left + 0 x) s))
          ((k-combinations k) m n)))

(define (sicp-solution n s) (matching-sums 3 1 n s))

; --------------------

(define (test n s)
  (printf "(test ~a ~a) -> ~a\n"
          n s (sicp-solution n s)))

(test 5 10)
(test 6 9)
(test 10 13)
