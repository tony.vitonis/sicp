(define (fold-both f init L)
  (printf "~a, ~a -> ~a, ~a\n"
          f L
          (fold-right f init L)
          (fold-left f init L)))

(fold-both / 1 '(1 2 3))
(fold-both list '() '(1 2 3))
(fold-both + 0 '(1 2 3))
(fold-both - 0 '(1 2 3))
(fold-both * 1 '(1 2 3))
