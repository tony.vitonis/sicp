(load "../math.scm")

; --------------------

(define (tree-map f tree)
  (map (lambda (subtree)
         (if (pair? subtree)
             (tree-map f subtree)
             (f subtree)))
       tree))

; --------------------

(define (square-tree tree) (tree-map square tree))

(printf "~a\n" (square-tree '(1 (2 (3 4) 5) (6 7))))
