(define (accumulate f init L)
  (if (null? L)
      init
      (f (car L)
         (accumulate f init (cdr L)))))

(define (count-leaves L)
  (accumulate (lambda (this so-far)
                (+ so-far (if (pair? this)
                              (count-leaves this)
                              1)))
              0 L))

; --------------------

(define (test L)
  (printf "~a -> ~a\n" L (count-leaves L)))

(test '((1 2) (3 4)))
(test '(((t u)) v (w) x (y z)))
(test '(a b c d e))
(test '())
