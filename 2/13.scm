(load "interval.scm")

; --------------------

(define (show name i)
  (printf "~a : ~a\n" name (interval-trepr i)))

(define a 1009) (define m 0.00173)
(define b 9973) (define n 0.00327)

(define x (center-tolerance a m))
(define y (center-tolerance b n))
(define p (interval* x y))

(show 'x x)
(show 'y y)
(show 'p p)
