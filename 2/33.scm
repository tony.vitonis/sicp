(define (accumulate f init L)
  (if (null? L)
      init
      (f (car L)
         (accumulate f init (cdr L)))))

(define (map f L)
  (accumulate (lambda (this so-far) (cons (f this) so-far))
              '() L))

(define (append L1 L2)
  (accumulate cons L2 L1))

(define (length L)
  (accumulate (lambda (this so-far) (+ 1 so-far))
              0 L))

(printf "~a\n" (map (lambda (x) (* x x x)) '(3/2 5)))
(printf "~a\n" (append '(1 2) '(3 4)))
(printf "~a\n" (length '(a b c d e)))
