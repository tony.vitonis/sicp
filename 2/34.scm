(define (accumulate f init L)
  (if (null? L)
      init
      (f (car L)
         (accumulate f init (cdr L)))))

(define (horner-eval x coefficient-sequence)
  (accumulate (lambda (this so-far)
                (+ this (* x so-far)))
              0
              coefficient-sequence))

(printf "~a\n" (horner-eval 2 '(1 3 0 5 0 1)))
