(define (point x y) (cons x y))

(define (x-of p) (car p))
(define (y-of p) (cdr p))

(define (point-string p)
  (format "(~a,~a)" (x-of p) (y-of p)))

; --------------------

(define (rectangle1 corner1 corner2)
  (let ((x1 (x-of corner1)) (x2 (x-of corner2))
        (y1 (y-of corner1)) (y2 (y-of corner2)))
    (cons (point (min x1 x2) (max y1 y2))
          (point (max x1 x2) (min y1 y2)))))

(define (upper-left1 r) (car r))
(define (lower-right1 r) (cdr r))

; --------------------

(define (rectangle2 upper-left width height)
  (cons upper-left (cons width height)))

(define (upper-left2 r) (car r))
(define (lower-right2 r)
  (point (+ (x-of (car r)) (cadr r))
         (- (y-of (car r)) (cddr r))))

; --------------------

(define (width r)
  (- (x-of (lower-right r))
     (x-of (upper-left r))))

(define (height r)
  (- (y-of (upper-left r))
     (y-of (lower-right r))))

(define (area r) (* (width r) (height r)))
(define (perimeter r) (* 2 (+ (width r) (height r))))

(define (rect-string r)
  (format "~a,~a" (point-string (upper-left r))
                  (point-string (lower-right r))))

; --------------------

(define (test r)
  (printf "Rectangle ... ~a\n" (rect-string r))
  (printf "Width ....... ~a\n" (width r))
  (printf "Height ...... ~a\n" (height r))
  (printf "Area ........ ~a\n" (area r))
  (printf "Perimeter ... ~a\n" (perimeter r)))

(define (print-separator)
  (printf "---------------------------\n"))

(define (test-all L)
  (define (iter L)
    (unless (null? L)
      (begin (print-separator)
             (test (car L))
             (iter (cdr L)))))
  (iter L)
  (print-separator))

(printf "Using rectangle1\n\n")

(define rectangle rectangle1)
(define upper-left upper-left1)
(define lower-right lower-right1)

(test-all (list (rectangle (point 1 7) (point 4 5))
                (rectangle (point 2 -2) (point -2 2))
                (rectangle (point -50/7 2/3) (point 83/3 2/7))))

(printf "\nUsing rectangle2\n\n")

(define rectangle rectangle2)
(define upper-left upper-left2)
(define lower-right lower-right2)

(test-all (list (rectangle (point 1 7) 3 2)
                (rectangle (point -2 2) 4 4)
                (rectangle (point -50/7 2/3) 731/21 8/21)))
