(load "../v.scm")  ; flatmap, prime?, range

; --------------------

(define (prime-sum? pair)
  (prime? (+ (car pair) (cadr pair))))

(define (prime-sum-pairs n)
  (if (>= n 2)
      (filter prime-sum? (flatmap unique-pairs (range 2 n)))
      (error 'prime-sum-pairs "argument not in domain" n)))

(define (unique-pairs i)
  (map (lambda (j) (list i j))
       (range (- i 1))))

; --------------------

(printf "~a\n" (prime-sum-pairs 6))
