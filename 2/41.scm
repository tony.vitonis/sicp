(load "../util.scm")  ; flatmap, range

; --------------------

(define (all-singles low high)
  (map list (range low high)))

(define (pairs low high)
  (map (lambda (x) (cons low x))
       (all-singles (+ low 1) high)))

(define (all-pairs low high)
  (flatmap (lambda (x) (pairs x high))
           (range low (- high 1))))

(define (triples low high)
  (map (lambda (x) (cons low x))
       (all-pairs (+ low 1) high)))

(define (all-triples low high)
  (flatmap (lambda (x) (triples x high))
           (range low (- high 2))))

(define (matching-sums n s)
  (filter (lambda (x) (= (fold-left + 0 x) s))
          (all-triples 1 n)))

; --------------------

(define (test n s)
  (printf "(test ~a ~a) -> ~a\n"
          n s (matching-sums n s)))

(test 5 10)
(test 6 9)
(test 10 13)
