(define (make-mobile left right) (list left right))
(define (make-branch length structure) (list length structure))

(define (left-of item) (car item))
(define (right-of item) (cadr item))

(load "29.scm")
