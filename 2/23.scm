(define (for-each f L)
  (unless (null? L)
    (begin (f (car L))
           (for-each f (cdr L)))))

(for-each (lambda (x) (newline) (display x))
          (list 57 321 88))
