(load "interval.scm")

; --------------------

(define (par1 r1 r2)
  (interval/ (interval* r1 r2)
             (interval+ r1 r2)))

(define (par2 r1 r2)
  (interval-1/ (interval+ (interval-1/ r1)
                          (interval-1/ r2))))

; --------------------

(define (test c1 t1 c2 t2)
  (let ((r1 (center-tolerance c1 t1))
        (r2 (center-tolerance c2 t2)))
    (printf "r1 = ~a\n" (interval-trepr r1))
    (printf "r2 = ~a\n" (interval-trepr r2))
    (printf "par1 -> ~a\n" (interval-trepr (par1 r1 r2)))
    (printf "par2 -> ~a\n" (interval-trepr (par2 r1 r2)))
    (newline)))

(test 0.01 0.91 0.01 0.91)
(test 0.01 0.91 0.02 0.91)

(test 100 0.01 100 0.01)
(test 100 0.01 200 0.02)
