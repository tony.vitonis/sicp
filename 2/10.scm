(load "interval.scm")

; --------------------

(define i0 (interval -1 1))
(define i1 (interval 1 3))

(define (test-div i j)
  (printf "~a/~a = ~a\n" (interval-repr i)
                         (interval-repr j)
                         (interval-repr (interval/ i j))))

(test-div i0 i1)
(test-div i1 i0)  ; No output - I don't know how to trap errors
(test-div i1 i1)
