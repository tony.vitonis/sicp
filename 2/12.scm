(load "interval.scm")

; --------------------

(define (test-selectors i)
  (let ((repr-i (interval-repr i)))
    (printf "Center of ~a .... ~a\n" repr-i (center i))
    (printf "Percent of ~a ... ~a\n" repr-i (percent i))
    (printf "Width of ~a ..... ~a\n" repr-i (width i))
    (newline)))

(define (test-mul x y)
  (printf "~a*~a = ~a\n" (interval-repr x)
                         (interval-repr y)
                         (interval-repr (interval* x y))))

(define -- (center-percent -3 5))
(define -+ (center-percent 1/2 200))
(define ++ (center-percent 7 10))

(test-selectors --)
(test-selectors -+)
(test-selectors ++)

(test-mul -- --)
(test-mul -- -+)
(test-mul -- ++) (newline)
(test-mul -+ --)
(test-mul -+ -+)
(test-mul -+ ++) (newline)
(test-mul ++ --)
(test-mul ++ -+)
(test-mul ++ ++)
