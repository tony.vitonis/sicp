(load "../math.scm")

; --------------------

(define (square-tree tree)
  (map (lambda (item)
         (if (pair? item)
             (square-tree item)
             (square item)))
       tree))

; --------------------

(printf "~a\n" (square-tree '(1 (2 (3 4) 5) (6 7))))
