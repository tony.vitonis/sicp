(load "interval.scm")

; --------------------

(define interval*-old interval*)

(define (interval* x y)
  (let ((Lx (lower-bound x)) (Ux (upper-bound x))
        (Ly (lower-bound y)) (Uy (upper-bound y)))
    (let ((-Lx (negative? Lx)) (-Ux (negative? Ux))
          (-Ly (negative? Ly)) (-Uy (negative? Uy)))
      (cond ((and -Ux -Uy)                              ; (-,-) (-,-)
             (interval (* Lx Ly) (* Ux Uy)))
            ((and -Ux -Ly)                              ; (-,-) (-,+)
             (interval (* Lx Uy) (* Lx Ly)))
            (-Ux                                        ; (-,-) (+,+)
             (interval (* Lx Uy) (* Ux Ly)))
            ((and -Lx -Uy)                              ; (-,+) (-,-)
             (interval (* Ux Ly) (* Lx Ly)))
            ((and -Lx -Ly)                              ; (-,+) (-,+)
             (interval (min (* Lx Uy) (* Ux Ly))
                       (max (* Lx Ly) (* Ux Uy))))
            (-Lx                                        ; (-,+) (+,+)
             (interval (* Lx Uy) (* Ux Uy)))
            (-Uy                                        ; (+,+) (-,-)
             (interval (* Ux Ly) (* Lx Uy)))
            (-Ly                                        ; (+,+) (-,+)
             (interval (* Ux Ly) (* Ux Uy)))
            (else                                       ; (+,+) (+,+)
             (interval (* Lx Ly) (* Ux Uy)))))))

; --------------------

(define (test-mul a b c d)
  (let ((x (interval a b))
        (y (interval c d)))
    (printf "old: ~a*~a = ~a\n"
            (interval-repr x) (interval-repr y)
            (interval-repr (interval*-old x y)))
    (printf "new: ~a*~a = ~a\n"
            (interval-repr x) (interval-repr y)
            (interval-repr (interval* x y)))
    (newline)))

(test-mul -11 -17 -31 -37)  ; (-,-) (-,-)
(test-mul -11 -17 -31  37)  ; (-,-) (-,+)
(test-mul -11 -17  31  37)  ; (-,-) (+,+)
(test-mul -11  17 -31 -37)  ; (-,+) (-,-)
(test-mul -11  17 -31  37)  ; (-,+) (-,+)
(test-mul -11  17  31  37)  ; (-,+) (+,+)
(test-mul  11  17 -31 -37)  ; (+,+) (-,-)
(test-mul  11  17 -31  37)  ; (+,+) (-,+)
(test-mul  11  17  31  37)  ; (+,+) (+,+)
