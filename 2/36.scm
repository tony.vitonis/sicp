(define (accumulate f init L)
  (if (null? L)
      init
      (f (car L)
         (accumulate f init (cdr L)))))

(define (accumulate-n f init Ls)
  (if (null? (car Ls))
      '()
      (cons (accumulate f init (map car Ls))
            (accumulate-n f init (map cdr Ls)))))

; --------------------

(define (test f init Ls)
  (printf "~a -> ~a\n" Ls (accumulate-n f init Ls)))

(test + 0 '((1 2 3) (4 5 6) (7 8 9) (10 11 12)))
(test * 1 '((1 2 3) (4 5 6) (7 8 9) (10 11 12)))
(test cons '() '((1 2 3) (4 5 6) (7 8 9) (10 11 12)))
