(define (last-pair L)
  (if (null? (cdr L))
      L
      (last-pair (cdr L))))

(define (test L)
  (printf "(last-pair ~a) -> ~a\n"
          L (last-pair L)))

(test (list 23 72 149 34)))
(test (list (list 1 2) (list 3 4) (list 5 6))))
