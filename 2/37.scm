(define (accumulate f init L)
  (if (null? L)
      init
      (f (car L)
         (accumulate f init (cdr L)))))

(define (accumulate-n f init Ls)
  (if (null? (car Ls))
      '()
      (cons (accumulate f init (map car Ls))
            (accumulate-n f init (map cdr Ls)))))

; --------------------

(define (make-matrix . vs) vs)

(define (dot-product v1 v2)
  (accumulate + 0 (map * v1 v2)))

(define (matrix-*-vector m v)
  (map (lambda (x) (dot-product x v)) m))

(define (transpose m)
  (accumulate-n cons '() m))

(define (matrix-*-matrix m1 m2)
  (let ((cols (transpose m2)))
    (map (lambda (v) (matrix-*-vector cols v)) m1)))

; --------------------
; Examples from https://mathinsight.org/matrix_vector_multiplication

(define m1 (make-matrix '(1 -1 2)
                        '(0 -3 1)))

(define v '(2 1 0))

(printf "~a\n" (matrix-*-vector m1 v))

(define m2 (make-matrix '(0 4 -2)
                        '(-4 -3 0)))

(define m3 (make-matrix '(0 1)
                        '(1 -1)
                        '(2 3)))

(printf "~a\n" (matrix-*-matrix m2 m3))
