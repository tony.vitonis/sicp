(define (make-mobile left right) (cons left right))
(define (make-branch length structure) (cons length structure))

(define (left-of item) (car item))
(define (right-of item) (cdr item))

(load "29.scm")
