(define (left-branch mobile) (left-of mobile))
(define (right-branch mobile) (right-of mobile))
(define (branch-length branch) (left-of branch))
(define (branch-structure branch) (right-of branch))

(define (mobile? item) (and (pair? item) (pair? (left-of item))))

(define (total-weight item)
  (if (mobile? item)
      (+ (total-weight (left-branch item))
         (total-weight (right-branch item)))
      (let ((structure (branch-structure item)))
        (if (mobile? structure)
            (total-weight structure)
            structure))))

(define (torque branch)
  (* (branch-length branch) (total-weight branch)))

(define (balanced? item)
  (or (not (mobile? item))
      (and (= (torque (left-branch item))
              (torque (right-branch item)))
           (balanced? (branch-structure (left-branch item)))
           (balanced? (branch-structure (right-branch item))))))

; --------------------

(define (test mobile)
  (printf "Mobile ......... ~a\n" mobile)
  (printf "Left branch .... ~a\n" (left-branch mobile))
  (printf "Right branch ... ~a\n" (right-branch mobile))
  (printf "Weight ......... ~a\n" (total-weight mobile))
  (printf "Balanced? ...... ~a\n" (balanced? mobile)))

(define (branch length s1 . s2)
  (make-branch length
               (if (null? s2)
                   s1
                   (make-mobile s1 (car s2)))))

(define LL (branch 3 6))
(define LR (branch 9 2))
(define L (branch 2 LL LR))
(define R (branch 4 4))
(define m (make-mobile L R))

(test m)
