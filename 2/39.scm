(define (reverse-from-right L)
  (fold-right (lambda (item so-far) (append so-far (list item))) '() L))

(define (reverse-from-left L)
  (fold-left (lambda (so-far item) (cons item so-far)) '() L))

; --------------------

(define (test L)
  (printf "~a\n" L)
  (printf "reverse-from-right: -> ~a\n" (reverse-from-right L))
  (printf "reverse-from-left:  -> ~a\n\n" (reverse-from-left L)))

(test '(1 2 3))
(test '(a b c d e f g h i j))
