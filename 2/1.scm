(define (make-rat n d)
  (define (same-sign? x y)
    (or (and (positive? x) (positive? y))
        (and (negative? x) (negative? y))))
  (let ((g (gcd n d))
        (op (if (same-sign? n d) + -)))
    (cons (op (abs (/ n g))) (abs (/ d g)))))

(define (numer x) (car x))
(define (denom x) (cdr x))

; --------------------

(define (add-rat x y)
  (make-rat (+ (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (sub-rat x y)
  (make-rat (- (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (mul-rat x y)
  (make-rat (* (numer x) (numer y))
            (* (denom x) (denom y))))

(define (div-rat x y)
  (make-rat (* (numer x) (denom y))
            (* (denom x) (numer y))))

(define (equal-rat? x y)
  (= (* (numer x) (denom y))
     (* (numer y) (denom x))))

(define (print-rat x)
  (printf "~a/~a\n" (numer x) (denom x)))

; --------------------

(define a (make-rat 1 2))
(define b (make-rat -3 4))
(define c (make-rat 5 -6))
(define d (make-rat -7 -8))

(display "Should be -1/4 ..... ") (print-rat (add-rat a b))
(display "Should be -41/24 ... ") (print-rat (sub-rat c d))
(display "Should be -5/12 .... ") (print-rat (mul-rat a c))
(display "Should be -6/7 ..... ") (print-rat (div-rat b d))
(display "Should be #f ....... ") (equal-rat? a b)
(display "Should be #t ....... ") (equal-rat? a (make-rat 91 182))
