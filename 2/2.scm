(define (average m n) (/ (+ m n) 2))

; --------------------

(define (make-point x y) (cons x y))
(define (x-point p) (car p))
(define (y-point p) (cdr p))

(define (make-segment p1 p2) (cons p1 p2))
(define (start-segment s) (car s))
(define (end-segment s) (cdr s))

(define (midpoint-segment s)
  (make-point (average (x-point (start-segment s))
                       (x-point (end-segment s)))
              (average (y-point (start-segment s))
                       (y-point (end-segment s)))))

(define (print-point p)
  (printf "(~a,~a)" (x-point p) (y-point p)))

(define (print-segment s)
  (print-point (start-segment s))
  (display " <-> ")
  (print-point (end-segment s)))

(define (print-midpoint s)
  (display "Midpoint of ") (print-segment s)
  (display " is ") (print-point (midpoint-segment s))
  (newline))

; --------------------

(define p1 (make-point 0 0))
(define p2 (make-point 2 2))
(define s1 (make-segment p1 p2))

(define p3 (make-point -5 4))
(define p4 (make-point 2 12))
(define s2 (make-segment p3 p4))
(define s3 (make-segment p4 p3))

(print-midpoint s1)
(print-midpoint s2)
(print-midpoint s3)
