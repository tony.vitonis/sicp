(load "interval.scm")

; --------------------

(define a (interval 1 2))
(define b (interval 3 4))
(define c (interval 5 6))

(define (show label i)
  (printf "~a = ~a, width ~a\n" label
                                (interval-repr i)
                                (width i)))

(show "a" a)
(show "b" b)
(show "c" c) (newline)

(show "a*b" (interval* a b))
(show "a*c" (interval* a c))
(show "a/b" (interval/ a b))
(show "a/c" (interval/ a c))
