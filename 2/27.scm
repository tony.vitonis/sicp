(define (deep-reverse items)
  (if (pair? items)
      (reverse (map deep-reverse items))
      items))

; --------------------

(define (test L)
  (printf "~a -> ~a\n"
          L (deep-reverse L)))

(test '())
(test '(()))
(test '(() ()))
(test '(a ((b c))))
(test '((1 2) (3 4)))
