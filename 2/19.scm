(define no-more? null?)
(define except-first-denomination cdr)
(define first-denomination car)

(define (cc amount coin-values)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (null? coin-values)) 0)
        (else (+ (cc amount
                     (except-first-denomination coin-values))
                 (cc (- amount (first-denomination coin-values))
                     coin-values)))))

; --------------------

(define us-coins '(50 25 10 5 1))
(define uk-coins '(100 50 20 10 5 2 1 0.5))

(define (test amount)
  (define (subtest label coins)
    (printf "~a ~a -> ~a\n~a (reversed) ~a -> ~a\n\n"
            label amount (cc amount coins)
            label amount (cc amount (reverse coins))))
  (subtest "U.S." us-coins)
  (subtest "U.K." uk-coins))

(test 11)
(test 100)
