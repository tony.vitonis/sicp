(load "../util.scm")  ; any?, flatmap, range

; Board representation: (a b c ...) where each element is the column of the corresponding row
; We have only to check vertical and diagonal threats; horizontal ones need > 1 queen per row

; --------------------

(define (diagonal-matches L)
  (let ((len (length L)))
    (if (< (length L) 2)
        '()
        (map (lambda (x)
               (list (- (car L) x) (+ (car L) x)))
             (range (- len 1))))))

(define (safe? board)
  (define (safe-vertical? L)
    (cond ((null? L) #t)
          ((memq (car L) (cdr L)) #f)
          (else (safe-vertical? (cdr L)))))
  (define (safe-diagonal? L)
    (cond ((null? L) #t)
          ((any? (map (lambda (x) (memq (car x) (cdr x)))
                      (map cons (cdr L) (diagonal-matches L))))
           #f)
          (else (safe-diagonal? (cdr L)))))
  (and (safe-vertical? board)
       (safe-diagonal? board)))

(define (safe-permutations s)
  (if (null? s)
      (list '())
      (flatmap (lambda (x)
                 (filter safe? (map (lambda (p) (cons x p))
                                    (safe-permutations (remove x s)))))
               s)))

(define (queens board-size)
  (if (zero? board-size)
      '()
      (safe-permutations (range board-size))))

; --------------------

(define (print-board board)
  (define (print-divider)
    (display "  ")
    (map display (map (lambda (x) "+---") (range (length board))))
    (display "+\n"))
  (define (print-row pos)
    (let ((before (- pos 1)) (after (- (length board) pos)))
      (define (print-blanks n)
        (when (> n 0) (display "|   ") (print-blanks (- n 1))))
      (print-divider) (display "  ")
      (print-blanks before) (display "| ■ ") (print-blanks after)
      (display "|\n")))
  (define (iter L)
    (unless (null? L)
      (begin (print-row (car L))
             (iter (cdr L)))))
  (printf "\n  ~a ->\n" board)
  (iter board)
  (print-divider))

; --------------------

(define (test f n) (print-board (f (queens n))))

(test car 8)
(test cadr 9)
(test caddr 10)
(test cadddr 11)
