(define (fringe L)
  (cond ((null? L) L)
        ((pair? (car L)) (append (fringe (car L)) (fringe (cdr L))))
        (else (cons (car L) (fringe (cdr L))))))

; --------------------

(define (test L)
  (printf "~a -> ~a\n" L (fringe L)))

(test '())
(test '(()))
(test '((())))
(test '(() ()))
(test '(() 1 2 3 () 4 5))
(test '(1 2 3 4 5))
(test '((1) 2 3 4 5))
(test '((1) ((((2 3) 4)) (5))))
(test '(1 2 3 4 (5)))

