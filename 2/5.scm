(define (mycons x y)
  (define (verified n)
    (when (not (positive? n)) (error 'mycons "values must be positive"))
    (when (not (integer? n)) (error 'mycons "values must be integers"))
    (inexact->exact n))
  (* (expt 2 (verified x)) (expt 3 (verified y))))

(define (mycar z)
  (define (iter n i)
    (if (= 0 (remainder n 2))
        (iter (/ n 2) (+ i 1))
        i))
  (iter z 0))

(define (mycdr z)
  (define (iter n i)
    (if (= 0 (remainder n 3))
        (iter (/ n 3) (+ i 1))
        i))
  (iter z 0))

; --------------------

(define x (mycons 2 3))
(display (list (mycar x) (mycdr x)))
(newline)

(define y (mycons 1 x))
(display (list (mycar y) (mycar (mycdr y)) (mycdr (mycdr y))))
(newline)
