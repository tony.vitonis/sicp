; Looking ahead and messing around

(load "interval.scm")

; --------------------

(define (symbol-append . L)
  (string->symbol (fold-left string-append "" (map symbol->string L))))

; --------------------

(define interval*-old interval*)

(define (interval* x y)
  (define (sign-of x) (if (negative? x) '- '+))
  (let* ((Lx (lower-bound x)) (Ux (upper-bound x))
         (Ly (lower-bound y)) (Uy (upper-bound y))
         (signs (symbol-append (sign-of Lx) (sign-of Ux)
                               (sign-of Ly) (sign-of Uy))))
    (cond ((eq? signs '----) (interval (* Lx Ly) (* Ux Uy)))
          ((eq? signs '---+) (interval (* Lx Uy) (* Lx Ly)))
          ((eq? signs '--++) (interval (* Lx Uy) (* Ux Ly)))
          ((eq? signs '-+--) (interval (* Ux Ly) (* Lx Ly)))
          ((eq? signs '-+-+) (interval (min (* Lx Uy) (* Ux Ly))
                                       (max (* Lx Ly) (* Ux Uy))))
          ((eq? signs '-+++) (interval (* Lx Uy) (* Ux Uy)))
          ((eq? signs '++--) (interval (* Ux Ly) (* Lx Uy)))
          ((eq? signs '++-+) (interval (* Ux Ly) (* Ux Uy)))
          ((eq? signs '++++) (interval (* Lx Ly) (* Ux Uy)))
          (else (error 'interval* "unexpected interval signs" signs)))))

; --------------------

(define (test a b c d)
  (let ((x (interval a b))
        (y (interval c d)))
    (printf "old: ~a*~a = ~a\n"
            (interval-repr x) (interval-repr y)
            (interval-repr (interval*-old x y)))
    (printf "new: ~a*~a = ~a\n"
            (interval-repr x) (interval-repr y)
            (interval-repr (interval* x y)))
    (newline)))

(test -11 -17 -31 -37)  ; (-,-) (-,-)
(test -11 -17 -31  37)  ; (-,-) (-,+)
(test -11 -17  31  37)  ; (-,-) (+,+)
(test -11  17 -31 -37)  ; (-,+) (-,-)
(test -11  17 -31  37)  ; (-,+) (-,+)
(test -11  17  31  37)  ; (-,+) (+,+)
(test  11  17 -31 -37)  ; (+,+) (-,-)
(test  11  17 -31  37)  ; (+,+) (-,+)
(test  11  17  31  37)  ; (+,+) (+,+)
